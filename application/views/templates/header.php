<html>

<head>
    <title>WIS Project</title>

    <!-- Bootstrap CDN: CSS & JS & JQuery-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>

    <!-- Dropzone CDN : CSS & JS -->
    <link href='https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/dropzone.css' type='text/css' rel='stylesheet'>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/dropzone.js' type='text/javascript'></script>

    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/style.css">
    <script type='text/javascript' src='<?php echo base_url(); ?>assets/js/script.js'></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script>
        var base_url = '<?php echo base_url(); ?>';
    </script>

    <script>
        // Add restrictions
        Dropzone.options.fileupload = {
            acceptedFiles: 'image/*',
            maxFilesize: 1, // MB
            addRemoveLinks: true,
            maxFiles: 1
        };
    </script>
    <script src='https://www.google.com/recaptcha/api.js'></script>

</head>

<body>
    <nav class="nav-extra fixed-top navbar navbar-expand-lg navbar-light">
        <h1 class="navbar-brand" href="#">A N Z E N</h1>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <?php if (isset($_SESSION['logged_in'])) : ?>
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="<?php echo site_url('Anzen_Home'); ?>">Home <span class="sr-only">(current)</span></a>
                    </li>
                </ul>
                <form class="form-inline my-2 my-lg-0">
                    <div class="nav-log-button">
                        <a class="btn btn-outline-success my-2 my-sm-0" href="<?php echo site_url('Anzen_Home/logout'); ?>" role="button">Logout</a>
                    </div>
                    <a class="btn btn-outline-primary my-2 my-sm-0" href="<?php echo site_url('Anzen_Home/profile'); ?>" role="button">Profile</a>
                </form>
            <?php else : ?>
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a>Welcome</a>
                    </li>
                </ul>
                <form class="form-inline my-2 my-lg-0">
                    <div class="nav-log-button">
                        <a class="btn btn-outline-success my-2 my-sm-0" href="<?php echo site_url('anzen'); ?>" role="button">Login</a>
                    </div>
                    <a class="btn btn-outline-primary my-2 my-sm-0" href="<?php echo site_url('anzen/reg_open'); ?>" role="button">Register</a>
                </form>
            <?php endif; ?>
        </div>
    </nav>

    <br><br><br>
    <div class="container-fluid">