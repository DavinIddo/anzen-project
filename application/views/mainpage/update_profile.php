
<div class="d-flex justify-content-center">
    <div class="card border-primary mb-3" style="max-width: 50rem;">
        <div class="card-body text-center">
            <h5 class="card-title">Update your profile!</h5>
            <h6 class="card-subtitle mb-2 text-muted">Card subtitle</h6>

            <br>
            <?php foreach($user as $data): ?>

            <?php if( isset($_SESSION['exists_status']) ): ?>
                <div class="alert alert-warning" role="alert">
                    <?php echo $_SESSION['exists_status']?>
                </div>
            <?php endif ?>

            <div class="text-danger"><?php echo validation_errors(); ?></div>

            <?php echo form_open("Anzen_Home/edit_profile"); ?>
                <div class="form-group">
                    <label for="UpdateUsername">Username</label>
                    <input type="text" name="update-username" class="form-control" id="UpdateUsername" placeholder="Enter username"
                        value="<?=$data['username']?>">
                    <br>
                    <br>
                    <label for="UpdateEmail">Email</label>
                    <input type="text" name="update-email" class="form-control" id="UpdateEmail" placeholder="Enter email"
                        value="<?=$data['email']?>">
                    <br>
                    <br>
                    <label for="UpdatePhone">Phone Number</label>
                    <input type="text" name="update-phone" class="form-control" id="UpdatePhone" placeholder="Enter phone number"
                        value="<?=$data['phone']?>">
                </div>
                <br>
                <p class="card-text text-danger">
                    Remember that your new Username/Email has to be unique, the same Username/Email can't be used twice.
                </p>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>

            <?php endforeach; ?>
        </div>
    </div>
</div>    