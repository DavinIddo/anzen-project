<!DOCTYPE html>

<head>
    <meta name="viewport" content="initial-scale=1.0">
    <meta charset="utf-8">
    <style>
        /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
        #map {
            height: 100%;
        }

        /* Optional: Makes the sample page fill the window. */
        html,
        body {
            height: 100%;
            margin: 0;
            padding: 0;
        }
    </style>
</head>

<body>
    <br><br>
    <div class="row">
        <div class="col-sm-7 table-event">
            <div class="card border-info mb-3">
                <div class="card-header">
                    <div class="row">
                        <div class="col">
                            <h5 class="card-class-title">Event List</h5>
                        </div>
                        <div class="col">
                            <a class="btn btn-primary add-event" href="<?php echo site_url('Anzen_Home/event_open'); ?>" role="button">Add Event</a>
                        </div>
                    </div>
                    <ul class="nav nav-tabs card-header-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="today-tab" data-toggle="tab" href="#today" role="tab">Current</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="upcoming-tab" data-toggle="tab" href="#upcoming" role="tab" 1>Future</a>
                        </li>
                    </ul>
                </div>
                <div class="tab-content">
                    <div class="card-body scroll tab-pane fade show active" id="today" role="tabpanel">
                        <h5 class="card-title">The Event is happening Today!</h5>
                        <ul class="list-group list-group-flush">

                            <?php if ($today_events != NULL) : ?>
                                <?php foreach ($today_events as $data) : ?>
                                    <li class="list-group-item">
                                        <?= $data['title'] ?>
                                        <a class="btn btn-danger rounded-pill float-right" role="button" href="<?php echo site_url('Anzen_Home/delete_event/' . $data['id']); ?>">Delete</a>

                                        <a class="btn btn-primary rounded-pill float-right" role="button" href="<?php echo site_url('Anzen_Home/event_detail/' . $data['id'] . '/' . $data['date']); ?>">Details</a>
                                    </li>
                                <?php endforeach; ?>

                            <?php else : ?>
                                <br>
                                <p class="card-text">There's no event that thou need to attend</p>
                            <?php endif ?>
                        </ul>
                    </div>

                    <div class="card-body scroll tab-pane fade" id="upcoming" role="tabpanel">
                        <h5 class="card-title">Event in the distant days</h5>
                        <ul class="list-group list-group-flush">

                            <?php if ($upcoming_events != NULL) : ?>
                                <?php foreach ($upcoming_events as $data) : ?>
                                    <li class="list-group-item">
                                        <?= $data['title'] ?>
                                        <a class="btn btn-danger rounded-pill float-right" role="button" href="<?php echo site_url('Anzen_Home/delete_event/' . $data['id']); ?>">Delete</a>

                                        <a class="btn btn-primary rounded-pill float-right" role="button" href="<?php echo site_url('Anzen_Home/event_detail/' . $data['id'] . '/' . $data['date']); ?>">Details</a>
                                    </li>
                                <?php endforeach; ?>
                            <?php else : ?>
                                <br>
                                <p class="card-text">There's no event that you need to attend in the future</p>
                            <?php endif ?>

                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-4">
            <div class="card border-info mb-3">
                <div class="card-body">
                    <h5 class="card-title">Past Events</h5>
                    <ul class="list-group list-group-flush">

                        <?php if ($past_events != NULL) : ?>
                            <?php foreach ($past_events as $data) : ?>
                                <li class="list-group-item">
                                    <?= $data['title'] ?>
                                    <a class="btn btn-danger rounded-pill float-right" role="button" href="<?php echo site_url('Anzen_Home/delete_event/' . $data['id']); ?>">Delete</a>

                                    <a class="btn btn-primary rounded-pill float-right" role="button" href="<?php echo site_url('Anzen_Home/event_review/' . $data['id'] . '/' . $data['date']); ?>">Review</a>
                                </li>
                            <?php endforeach; ?>
                        <?php else : ?>
                            <br>
                            <p class="card-text">Whoooosh, it's empty</p>
                        <?php endif ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <br>

    <div class="container">
        <div id="map"></div>
    </div>
    <script>
        var map, infoWindow;

        function initMap() {
            var mapOption = {
                center: {
                    lat: -34.397,
                    lng: 150.644
                },
                zoom: 18
            }
            map = new google.maps.Map(document.getElementById('map'), mapOption);

            /// Setup GEOLOCATION with Marker///
            infoWindow = new google.maps.InfoWindow;

            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function(position) {
                    var marker = new google.maps.Marker({
                        position: {
                            lat: position.coords.latitude,
                            lng: position.coords.longitude
                        },
                        map: map
                    });

                    var pos = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    };

                    infoWindow.setPosition(pos);
                    infoWindow.setContent('User Location');
                    map.setCenter(pos);

                    google.maps.event.addListener(marker, 'click', function() {
                        infoWindow.open(map, marker);
                    });
                }, function() {
                    handleLocationError(true, infoWindow, map.getCenter());
                });
            } else {
                // Browser doesn't support Geolocation
                handleLocationError(false, infoWindow, map.getCenter());
            }
        }

        function handleLocationError(browserHasGeolocation, infoWindow, pos) {
            infoWindow.setPosition(pos);
            infoWindow.setContent(browserHasGeolocation ?
                'Error: The Geolocation service failed.' :
                'Error: Your browser doesn\'t support geolocation.');
            infoWindow.open(map);
        }

        google.maps.event.addDomListener(window, 'load', initialize);
    </script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAgfu7T2RAXnK2VW5B9OaatbeH7WeY3Q9A&callback=initMap"></script>


    <br>
    <a href="http://[::1]/project/index.php/anzen/">Back to login</a>