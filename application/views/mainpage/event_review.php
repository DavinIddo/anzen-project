<br>
<h2 class="text-success text-center">REVIEW EVENT</h2>
<br>

<div class="container">
    <?php if ($_SESSION['greet']) : ?>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <h4 class="alert-heading">Hey!</h4>
            <p>Would you like to rate how well the event went? Maybe leave a comment to remind you what happen on that special day for the foreseeable future? It's all up to you my friend.</p>
            <hr>
            <p class="mb-0">Go to the bottom of the page to start creating it if you are interested.</p>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    <?php endif ?>
</div>

<br>
<div class="row">
    <div class="col">
        <div class="card text-white bg-primary mb-3">
            <div class="card-body text-center">
                <p class="card-text">
                    <?php foreach ($event as $data) : ?>
                        <form class="form-horizontal">
                            <div class="form-group">
                                <label class="col">Title :</label>
                                <div class="col">
                                    <h2 class="form-control-static"><?= $data['title'] ?></h2>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col">Date :</label>
                                <div class="col">
                                    <h2 class="form-control-static"><?= $data['date'] ?></h2>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label class="col">Time Start :</label>
                                        <div class="col">
                                            <h2 class="form-control-static"><?= $data['time-start'] ?></h2>
                                        </div>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label class="col">Time End :</label>
                                        <div class="col">
                                            <h2 class="form-control-static"><?= $data['time-end'] ?></h2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col">Description :</label>
                                <div class="col">
                                    <h2 class="form-control-static"><?= $data['description'] ?></h2>
                                </div>
                            </div>

                            <br>
                        </form>
                    <?php endforeach; ?>
                </p>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <?php echo $calendar; ?>
    </div>
</div>

<br><br><br>
<div class="rate-start">
    <?php if ($rating == NULL) : ?>

        <h3>Rate how your event goes!</h3><br>

        <?php echo form_open("Anzen_Home/write_rating/" . $event_id . '/' . $event_date); ?>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="RadioOption" id="inlineRadio1" value="1*">
            <label class="form-check-label" for="inlineRadio1">1*</label>
        </div>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="RadioOption" id="inlineRadio2" value="2*">
            <label class="form-check-label" for="inlineRadio2">2*</label>
        </div>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="RadioOption" id="inlineRadio3" value="3*">
            <label class="form-check-label" for="inlineRadio3">3*</label>
        </div>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="RadioOption" id="inlineRadio4" value="4*">
            <label class="form-check-label" for="inlineRadio4">4*</label>
        </div>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="RadioOption" id="inlineRadio5" value="5*">
            <label class="form-check-label" for="inlineRadio5">5*</label>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
        </form>

    <?php else : ?>
        <?php foreach ($rating as $temp) : ?>
            <h3>CHECK OUT YOUR RATING!</h3><br>

            <p>YOU RATE THIS EVENT WITH A </p>

            <h2><?php echo $temp['rating'] ?> RATING</h2>
            <br>
            <a href="<?php echo site_url('Anzen_Home/delete_rating/' . $temp['id'] . '/' . $event_id . '/' . $event_date); ?>" class="btn btn-warning">
                Change
            </a>
        <?php endforeach ?>
    <?php endif ?>
</div>

<br><br>
<h3 class="comment-start">COMMENTS</h3>
<br>

<div class="container">
    <p>
        <a class="btn btn-success" data-toggle="collapse" href="#collapseForm" role="button" aria-expanded="false" aria-controls="collapseExample">
            Create your Comment here (click me!)
        </a>
    </p>

    <div class="collapse" id="collapseForm">
        <div class="card card-body comment">

            <?php echo form_open("Anzen_Home/write_comment/" . $event_id . '/' . $event_date); ?>
            <div class="form-group">
                <?php if (isset($_SESSION['error_comment'])) : ?>
                    <div class="alert alert-warning alert-dismissible fade show" role="alert">
                        <?php echo $_SESSION['error_comment']; ?>
                    </div>
                <?php endif ?>

                <label for="EventTitle">Title</label>
                <input type="text" name="comment-title" class="form-control" id="CommentTitle" placeholder="Enter the comment title" value="<?php echo set_value('comment-title'); ?>">
                <br>
                <label for="EventDescription">Description</label>
                <input type="text" name="comment-description" class="form-control" id="CommentDescription" placeholder="Enter the comment description" value="<?php echo set_value('comment-description'); ?>">
            </div>

            <button type="submit" class="btn btn-primary">Submit</button>
            </form>

        </div>
    </div>

    <br>
    <?php if ($comments == NULL) : ?>
        <br>
        <h4 class="nothing">There's nothing here...</h4>

        <?php else :
        foreach ($comments as $temp) : ?>

            <div class="card">
                <div class="card-body">
                    <h5 class="card-title"><?php echo $temp['title'] ?></h5>
                    <p class="card-text"><?php echo $temp['description'] ?></p>
                    <br>
                    <p>Created at <?php echo $temp['date'] ?></p>
                    <a href="<?php echo site_url('Anzen_Home/delete_comment/' . $temp['id'] . '/' . $event_id . '/' . $event_date); ?>" class="btn btn-danger">
                        Delete
                    </a>
                </div>
            </div>

        <?php endforeach ?>
        <br><br>
    <?php endif ?>
</div>