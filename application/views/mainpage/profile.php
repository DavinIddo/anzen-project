<h3 class="text-center">Welcome to your profile!</h3>
<br>
<br>

<div class="container">
    <div class="row">
        <div class="col">
            <?php foreach ($user as $data) : ?>

                <form class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-10 control-label">Username :</label>
                        <div class="col-sm-10">
                            <h2 class="form-control-static"><?= $data['username'] ?></h2>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-10 control-label">Email :</label>
                        <div class="col-sm-10">
                            <h2 class="form-control-static"><?= $data['email'] ?></h2>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-10 control-label">Phone number :</label>
                        <div class="col-sm-10">

                            <?php if (!$data['phone'] == '') : ?>
                                <h2 class="form-control-static"><?= $data['phone'] ?></h2>
                            <?php else : ?>
                                <h2 class="form-control-static">N/A</h2>
                            <?php endif ?>

                        </div>
                    </div>

                    <br>
                    <a class="btn btn-primary" href="<?php echo site_url('Anzen_Home/edit_open'); ?>" role="button">Edit Profile</a>
                </form>


        </div>

        <div class="col">
            <?php $source = '/project/uploads/' . $data['image']; ?>
            <img src="<?php echo $source; ?>" class="rounded mx-auto d-block" alt="image">

        <?php endforeach; ?>
        <br>

        <div class="d-flex justify-content-center">

            <?php if ($_SESSION['upload_error'] != ' ') : ?>

                <?php foreach ($_SESSION['upload_error'] as $error) : ?>
                    <div class="alert alert-warning" role="alert">
                        <?php echo $error; ?>
                    </div>
                <?php endforeach ?>
                <?php unset($_SESSION['upload_error']); ?>

            <?php else :
                echo $_SESSION['upload_error'];
                unset($_SESSION['upload_error']);
            ?>
            <?php endif ?>

            <div class="row">
                <div class="col">
                    <?php echo form_open_multipart('Anzen_Home/do_upload'); ?>

                    <input type="file" name="userfile" size="20" />

                    <br /><br />

                    <input type="submit" value="upload" />

                    </form>
                </div>
                <div class="col">
                    <div class='content'>
                        <!-- Dropzone -->
                        <form action="<?= site_url('Anzen_Home/file_upload') ?>" class="dropzone" id="fileupload">

                            <input type="submit" value="upload" />

                        </form>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
</div>