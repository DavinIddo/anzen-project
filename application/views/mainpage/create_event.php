<h3 class="text-success text-center">CREATE EVENT</h3>

<div class="text-danger"><?php echo validation_errors(); ?></div>

<?php echo form_open("Anzen_Home/add_event"); ?>
<div class="form-group">
    <label for="EventTitle">Title <a class="text-danger">*</a> </label>
    <input type="text" name="event-title" class="form-control" id="EventTitle" placeholder="Enter the event title" value="<?php echo set_value('event-title'); ?>">
    <br>
    <label for="EventDate">Date <a class="text-danger">*</a> </label>
    <input type="date" name="event-date" class="form-control" id="EventDate" placeholder="Enter the date" value="<?php echo set_value('event-date'); ?>">
    <br>
    <div class="row">
        <div class="col">
            <label for="EventTimeStart">Time Start <a class="text-danger">*</a> </label>
            <input type="time" name="event-time-start" class="form-control" id="EventTimeStart" value="<?php echo set_value('event-time-start'); ?>">
        </div>
        <div class="col">
            <label for="EventTimeEnd">Time End <a class="text-danger">*</a> </label>
            <input type="time" name="event-time-end" class="form-control" id="EventTimeEnd" value="<?php echo set_value('event-time-end'); ?>">
        </div>
    </div>
    <br>
    <label for="EventDescription">Description <a class="text-danger">*</a> </label>
    <input type="text" name="event-description" class="form-control" id="EventDescription" placeholder="Enter the event description" value="<?php echo set_value('event-description'); ?>">
</div>

<button type="submit" class="btn btn-primary">Submit</button>
</form>