<br>
<h2 class="text-success text-center">EVENT DETAIL</h2>
<br><br>

<div class="row">
    <div class="col-sm-6">
        <div class="card text-white bg-primary mb-3">
            <div class="card-body text-center">
                <p class="card-text">
                    <?php foreach ($event as $data) : ?>
                        <form class="form-horizontal">
                            <div class="form-group">
                                <label class="col">Title :</label>
                                <div class="col">
                                    <h2 class="form-control-static"><?= $data['title'] ?></h2>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col">Date :</label>
                                <div class="col">
                                    <h2 class="form-control-static"><?= $data['date'] ?></h2>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label class="col">Time Start :</label>
                                        <div class="col">
                                            <h2 class="form-control-static"><?= $data['time-start'] ?></h2>
                                        </div>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label class="col">Time End :</label>
                                        <div class="col">
                                            <h2 class="form-control-static"><?= $data['time-end'] ?></h2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col">Description :</label>
                                <div class="col">
                                    <h2 class="form-control-static"><?= $data['description'] ?></h2>
                                </div>
                            </div>

                            <br>
                        </form>
                    <?php endforeach; ?>
                </p>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <?php echo $calendar; ?>
    </div>
</div>