<h1 class="text-info text-center">A N Z E N</h1>

<div class="container">
  <?php if (isset($_SESSION['login_status'])) : ?>
    <div class="alert alert-warning" role="alert">
      <?php echo $_SESSION['login_status'] ?>
    </div>
  <?php endif ?>

  <br>

  <div class="text-danger"><?php echo validation_errors(); ?></div>

  <?php echo form_open("anzen/login"); ?>
  <div class="form-group">
    <label for="LoginUsername">Username</label>
    <input type="text" name="username" class="form-control" id="LoginUsername" placeholder="Enter username" value="<?php echo set_value('username'); ?>">
    <br>
    <label for="LoginEmail">Email</label>
    <input type="text" name="email" class="form-control" id="LoginEmail" placeholder="Enter email" value="<?php echo set_value('email'); ?>">
    <br>
    <label for="LoginPassword">Password</label>
    <input type="password" name="password" class="form-control" id="LoginPassword" placeholder="Password" value="<?php echo set_value('password'); ?>">
    <br>
    <input type="checkbox" class="form-check-input" id="check" name="check">
    <label class="form-check-label" for="check">Remember Me!</label>
  </div>

  <br>
  <div class="row">
    <div class="col">
      <button type="submit" class="btn btn-success">Submit</button>
    </div>

    <div class="col">
      <a class="btn btn-warning float-right" href="<?php echo site_url('anzen/rec_open'); ?>" role="button">Forgot Password?</a>
      <a class="btn btn-warning" href="<?php echo site_url('anzen/resend_open'); ?>" role="button">Re-send Email</a>
    </div>
  </div>
  </form>
</div>