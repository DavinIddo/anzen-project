<h3 class="text-success text-center">PASSWORD RECOVERY</h3>

<div class="container">
  <?php if (isset($_SESSION['recovery_status'])) : ?>
    <div class="alert alert-warning" role="alert">
      <?php echo $_SESSION['recovery_status'] ?>
    </div>
  <?php endif ?>

  <div class="text-danger"><?php echo validation_errors(); ?></div>

  <?php echo form_open("anzen/recovery"); ?>
  <div class="form-group">
    <label for="RecoveryUsername">Username</label>
    <input type="text" name="recovery-username" class="form-control" id="RecoveryUsername" placeholder="Enter your username" value="<?php echo set_value('recovery-username'); ?>">
    <br>
    <label for="RecoveryEmail">Email</label>
    <input type="text" name="recovery-email" class="form-control" id="RecoveryEmail" placeholder="Enter your email" value="<?php echo set_value('recovery-email'); ?>">
    <br>
    <label for="RecoveryQuestion">Security Question</label>
    <input type="text" name="recovery-question" class="form-control" id="RecoveryQuestion" placeholder="Enter your security question" value="<?php echo set_value('recovery-question'); ?>">
    <br>
    <label for="RecoveryAnswer">Security Answer</label>
    <input type="text" name="recovery-answer" class="form-control" id="RecoveryAnswer" placeholder="Enter your security answer" value="<?php echo set_value('recovery-answer'); ?>">
    <br>
    <label for="captImg">Captcha</label>
    <p>Image unclear? Click <a href="#" class="refreshCaptcha">refresh</a></p>
    <p id="captImg" class="captcha-image"><?php echo $captImage ?></p>
    <input type="text" name="recovery-captcha">
  </div>

  <button type="submit" class="btn btn-primary">Submit</button>
  </form>
</div>
