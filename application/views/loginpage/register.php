<h1 class="text-primary text-center">REGISTER FORM</h1>

<div class="container">
  <?php if (isset($_SESSION['exists_status'])) : ?>
    <div class="alert alert-warning" role="alert">
      <?php echo $_SESSION['exists_status'] ?>
    </div>
  <?php endif ?>

  <div class="text-danger"><?php echo validation_errors(); ?></div>

  <?php echo form_open("anzen/registration"); ?>
  <div class="form-group">
    <label for="RegisterUsername">Username (min. 5 characters) <a class="text-danger">*</a> </label>
    <input type="text" name="register-username" class="form-control" id="RegisterUsername" placeholder="Enter username" value="<?php echo set_value('register-username'); ?>">
    <br>
    <label for="RegisterEmail">Email <a class="text-danger">*</a> </label>
    <input type="email" name="register-email" class="form-control" id="RegisterEmail" placeholder="Enter email" value="<?php echo set_value('register-email'); ?>">
    <br>
    <label for="RegisterPhoneNum">Phone Number</label>
    <input type="text" name="register-phone" class="form-control" id="RegisterPhoneNum" placeholder="Enter your phone number" value="<?php echo set_value('register-phone'); ?>">
    <br>
    <label for="RegisterQuestion">Security Question <a class="text-danger">*</a> </label>
    <input type="text" name="reg-security-question" class="form-control" id="RegisterQuestion" placeholder="Enter your security question" value="<?php echo set_value('reg-security-question'); ?>">
    <br>
    <label for="RegisterAnswer">Security Answer <a class="text-danger">*</a> </label>
    <input type="text" name="reg-security-answer" class="form-control" id="RegisterAnswer" placeholder="Enter your security answer" value="<?php echo set_value('reg-security-answer'); ?>">
    <br>
    <label for="RegisterPassword">Password (min. 6 characters) <a class="text-danger">*</a> </label>
    <input type="password" name="register-password" class="form-control" id="RegisterPassword" placeholder="Enter Password" value="<?php echo set_value('register-password'); ?>">
    <br>
    <label for="ConfirmPassword">Confirm Password <a class="text-danger">*</a> </label>
    <input type="password" name="confirm-password" class="form-control" id="ConfirmPassword" placeholder="Enter confirmation Password" value="<?php echo set_value('confirm-password'); ?>">
    <br>
    <div class="g-recaptcha" data-sitekey="6Ldluf0UAAAAALWfkgSdNTnJC-MPd_srNdJl6xIr"></div>
  </div>
  <br>
  <button type="submit" class="btn btn-primary">Submit</button>
  </form>
</div>