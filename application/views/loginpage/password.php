<h3 class="text-success text-center">FORM YOUR NEW PASSWORD</h3>

<div class="container">
  <?php if (isset($_SESSION['password_status'])) : ?>
    <div class="alert alert-warning" role="alert">
      <?php echo $_SESSION['password_status'] ?>
    </div>
  <?php endif ?>

  <div class="text-danger"><?php echo validation_errors(); ?></div>

  <?php echo form_open("anzen/password_renewal"); ?>
  <div class="form-group">
    <label for="NewPassword">Password (min. 6 characters)</label>
    <input type="password" name="new-password" class="form-control" id="NewPassword" placeholder="Enter your new Password">
    <br>
    <label for="ConfirmNewPassword">Confirm Password</label>
    <input type="password" name="confirm-new-password" class="form-control" id="ConfirmNewPassword" placeholder="Enter confirmation Password">
  </div>

  <button type="submit" class="btn btn-primary">Submit</button>
  </form>
</div>