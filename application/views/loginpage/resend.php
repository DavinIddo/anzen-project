<h3 class="text-success text-center">RESEND EMAIL VERIFICATION</h3>

<br><br>
<div class="container">
    <?php if (isset($_SESSION['exists_status'])) : ?>
        <div class="alert alert-warning" role="alert">
            <?php echo $_SESSION['exists_status'] ?>
        </div>
    <?php endif ?>

    <div class="text-danger"><?php echo validation_errors(); ?></div>

    <?php echo form_open("anzen/resend_email"); ?>
    <div class="form-group">
        <label for="ResendEmail">Email</label>
        <input type="text" name="resend-email" class="form-control" id="ResendEmail" placeholder="Enter your email" value="<?php echo set_value('resend-email'); ?>">
    </div>

    <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>