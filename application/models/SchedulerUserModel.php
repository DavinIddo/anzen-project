<?php
defined('BASEPATH') or exit('No direct script access allowed');

class SchedulerUserModel extends CI_Model
{

    public function get_user()
    {
        $query = $this->mongo_db->get('User');

        return $query;
    }

    public function get_the_user()
    {
        $query = $this->mongo_db->get_where(
            'User',
            array(
                'username' => $_SESSION['username'],
                'email' => $_SESSION['email']
            )
        );

        return $query;
    }

    public function check_user_exists($username, $email)
    {
        $query = $this->mongo_db->get_where(
            'User',
            array(
                'username' => $username,
                'email' => $email
            )
        );

        return $query;
    }

    public function username_exists($input)
    {
        $query = $this->mongo_db->get_where(
            'User',
            array(
                'username' => $input
            )
        );

        return $query;
    }

    public function email_exists($input)
    {
        $query = $this->mongo_db->get_where(
            'User',
            array(
                'email' => $input
            )
        );

        return $query;
    }

    public function insert_user($hashed_password)
    {
        $data = array(
            'username' => $this->input->post('register-username'),
            'email' => $this->input->post('register-email'),
            'password' => $hashed_password,
            'phone' => $this->input->post('register-phone'),
            'image' => 'placeholder.png',
            'security-question' => strtolower($this->input->post('reg-security-question')),
            'security-answer' => strtolower($this->input->post('reg-security-answer')),
            'auth_status' => FALSE
        );

        return $this->mongo_db->insert('User', $data);
    }

    public function update_user($change, $old, $new)
    {
        $this->mongo_db->where($change, $old);
        $this->mongo_db->set($change, $new);
        $this->mongo_db->update('User');
    }

    public function update_image($change, $placement, $new)
    {
        $this->mongo_db->where('username', $placement);
        $this->mongo_db->set($change, $new);
        $this->mongo_db->update('User');
    }

    public function update_user_detail($place, $place_info, $change, $new)
    {
        $this->mongo_db->where($place, $place_info);
        $this->mongo_db->set($change, $new);
        $this->mongo_db->update('User');
    }

    public function delete_user()
    {
        $this->mongo_db->where('username', $this->input->post('username'));
        $this->mongo_db->delete('User');
    }
}
