<?php
defined('BASEPATH') or exit('No direct script access allowed');

class SchedulerCommentModel extends CI_Model
{

    public function insert_comment($eventId)
    {
        $data = array(
            'id' => uniqid(),
            'event_id' => $eventId,
            'title' => $this->input->post('comment-title'),
            'description' => $this->input->post('comment-description'),
            'date' => date("Y-m-d"),
        );

        return $this->mongo_db->insert('Comment', $data);
    }

    public function get_all_comments($eventId)
    {
        $query = $this->mongo_db->get_where('Comment',
            array(
                'event_id' => $eventId
            )
        );

        return $query;
    }

    public function del_comment($id)
    {
        $this->mongo_db->where('id', $id);
        $this->mongo_db->delete('Comment');
    }

}
