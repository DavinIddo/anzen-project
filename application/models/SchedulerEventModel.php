<?php
defined('BASEPATH') or exit('No direct script access allowed');

class SchedulerEventModel extends CI_Model
{

    public function insert_event()
    {
        $data = array(
            'id' => uniqid(),
            'username' => $_SESSION['username'],
            'title' => $this->input->post('event-title'),
            'date' => $this->input->post('event-date'),
            'time-start' => $this->input->post('event-time-start'),
            'time-end' => $this->input->post('event-time-end'),
            'description' => $this->input->post('event-description')
        );

        return $this->mongo_db->insert('Event', $data);
    }

    public function get_all_events()
    {
        $query = $this->mongo_db->get_where('Event',
            array(
                'username' => $_SESSION['username']
            )
        );

        return $query;
    }

    public function get_event($id)
    {
        $query = $this->mongo_db->get_where('Event',
            array(
                'id' => $id
            )
        );

        return $query;
    }

    public function del_event($id)
    {
        $this->mongo_db->where('id', $id);
        $this->mongo_db->delete('Event');
    }

}
