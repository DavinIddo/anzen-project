<?php
defined('BASEPATH') or exit('No direct script access allowed');

class SchedulerRatingModel extends CI_Model
{
    public function insert_rating($eventId)
    {
        $data = array(
            'id' => uniqid(),
            'event_id' => $eventId,
            'rating' => $this->input->post('RadioOption')
        );

        return $this->mongo_db->insert('Rating', $data);
    }

    public function get_rating($eventId)
    {
        $query = $this->mongo_db->get_where('Rating',
            array(
                'event_id' => $eventId
            )
        );

        return $query;
    }

    public function update_rating($eventId)
    {
        $new_rating = $this->input->post('RadioOption');

        $this->mongo_db->where('event_id', $eventId);
        $this->mongo_db->set('rating', $new_rating);
        $this->mongo_db->update('Rating');
    }

    public function del_rating($id)
    {
        $this->mongo_db->where('id', $id);
        $this->mongo_db->delete('Rating');
    }

}
