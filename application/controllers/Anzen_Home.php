<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Anzen_Home extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if ($this->input->cookie('username') != NULL && $this->input->cookie('email') != NULL) {
            $userdata = array(
                'username' => $this->input->cookie('username'),
                'email' => $this->input->cookie('email'),
                'logged_in' => TRUE
            );

            $this->session->set_userdata($userdata);
            $this->session->set_tempdata('timeout', TRUE, 60);
        }

        if (!$this->session->userdata('logged_in')) {

            $allowed = array(
                'logout'
            );

            if (!in_array($this->router->fetch_method(), $allowed)) {
                redirect('anzen');
            }
        }

        if ($_SESSION['timeout'] != NULL) {
            $this->session->unset_userdata('timeout');
            $this->session->set_tempdata('timeout', TRUE, 600);
        } else {
            $this->session->set_flashdata('login_status', "NOTIFICATION: Logout automatically due to inactivity");

            $array_items = array('username', 'email', 'logged_in');
            $this->session->unset_userdata($array_items);

            if ($this->input->cookie('username') != NULL && $this->input->cookie('email') != NULL) {
                $this->input->set_cookie("username", '', time() - 1);
                $this->input->set_cookie("email", '', time() - 1);
            }

            redirect('Anzen');
        }

        $this->load->model('SchedulerEventModel');
        $this->load->model('SchedulerUserModel');
        $this->load->model('SchedulerCommentModel');
        $this->load->model('SchedulerRatingModel');
    }

    public function index()
    {
        $database = $this->SchedulerEventModel->get_all_events();

        $data['today_events'] = array();
        $data['upcoming_events'] = array();
        $data['past_events'] = array();

        foreach ($database as $temp) {
            if (date("Y-m-d") == $temp['date'])
                array_push($data['today_events'], $temp);
            else if (date("Y-m-d") < $temp['date'])
                array_push($data['upcoming_events'], $temp);
            else
                array_push($data['past_events'], $temp);
        }

        $this->load->view('templates/header');
        $this->load->view('mainpage/main', $data);
        $this->load->view('templates/footer');
    }

    public function profile()
    {
        $data['user'] = $this->SchedulerUserModel->get_the_user();

        if (!isset($_SESSION['upload_error'])) {
            $this->session->set_flashdata('upload_error', ' ');
        } else {
            $this->session->set_flashdata('upload_error', $_SESSION['upload_error']);
        }

        $this->load->view('templates/header');
        $this->load->view('mainpage/profile', $data);
        $this->load->view('templates/footer');
    }

    public function do_upload()
    {
        $config['upload_path']          = './uploads/';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $config['max_size']             = '1024';
        $config['max_width']            = 320;
        $config['max_height']           = 320;

        $this->load->library('upload', $config);


        // File is not uploaded if there's an error
        if (!$this->upload->do_upload('userfile')) {
            $error = array('error' => $this->upload->display_errors());
            $this->session->set_flashdata('upload_error', $error);

            redirect('Anzen_Home/profile');

            // File uploaded
        } else {
            $this->SchedulerUserModel->update_image('image', $_SESSION['username'], $this->upload->data('file_name'));

            redirect('Anzen_Home/profile');
        }
    }

    // Solution taken from here https://makitweb.com/drag-and-drop-file-upload-with-dropzone-in-codeigniter/
    public function file_upload()
    {
        if (!empty($_FILES['file']['name'])) {

            // Set preference
            $config['upload_path'] = 'uploads/';
            $config['allowed_types'] = 'jpg|jpeg|png|gif';
            $config['max_size'] = '1024';
            $config['file_name'] = $_FILES['file']['name'];

            //Load upload library
            $this->load->library('upload', $config);

            // File upload
            if ($this->upload->do_upload('file')) {
                $this->SchedulerUserModel->update_image('image', $_SESSION['username'], $this->upload->data('file_name'));

                redirect('Anzen_Home/profile');
            }

            redirect('Anzen_Home/profile');
        }

        redirect('Anzen_Home/profile');
    }

    public function edit_open()
    {
        $data['user'] = $this->SchedulerUserModel->get_the_user();

        $this->load->view('templates/header');
        $this->load->view('mainpage/update_profile', $data);
        $this->load->view('templates/footer');
    }

    public function edit_profile()
    {
        $this->load->library('form_validation');

        $config = array(
            array(
                'field' => 'update-username',
                'label' => 'Username',
                'rules' => 'required|min_length[5]',
                'errors' => array(
                    'required' => 'You have not provided %s.'
                )
            ),
            array(
                'field' => 'update-email',
                'label' => 'Email',
                'rules' => 'required|valid_email',
                'errors' => array(
                    'required' => 'You have not provided %s.'
                )
            ),
            array(
                'field' => 'update-phone',
                'label' => 'Phone Number',
                'rules' => 'numeric',
                'errors' => array(
                    'numeric' => '%s has to be a number'
                )
            )
        );

        $this->form_validation->set_rules($config);

        $newdata = array(
            'username' => $this->input->post('update-username'),
            'email' => $this->input->post('update-email'),
            'phone' => $this->input->post('update-phone')
        );

        $existedata = array(
            'username' => $this->SchedulerUserModel->username_exists($newdata['username']),
            'email' => $this->SchedulerUserModel->email_exists($newdata['email'])
        );

        foreach (['username', 'email'] as $tempdata) {

            if (!($_SESSION[$tempdata] == $newdata[$tempdata])) {

                if (!($existedata[$tempdata] == NULL)) {
                    $this->session->set_flashdata('exists_status', 'ERROR: Username and/or Email has already exists!');
                    redirect('Anzen_Home/edit_open');
                }
            }
        }

        if (!isset($_SESSION['exists_status'])) {

            if ($this->form_validation->run() == FALSE) {
                $data['user'] = $this->SchedulerUserModel->get_the_user();

                $this->load->view('templates/header');
                $this->load->view('mainpage/update_profile', $data);
                $this->load->view('templates/footer');
            } else {
                $data['user'] = $this->SchedulerUserModel->get_the_user();

                foreach ($data['user'] as $item) {
                    $data['username'] = $item['username'];
                    $data['email'] = $item['email'];
                    $data['phone'] = $item['phone'];
                }

                foreach (['username', 'email', 'phone'] as $check) {
                    $userdata[$check] = $newdata[$check];

                    $this->SchedulerUserModel->update_user($check, $data[$check], $newdata[$check]);
                }
                $this->session->set_userdata($userdata);

                redirect('Anzen_Home/profile');
            }
        }
    }

    public function event_open()
    {
        $this->load->view('templates/header');
        $this->load->view('mainpage/create_event');
        $this->load->view('templates/footer');
    }

    public function add_event()
    {
        $this->load->library('form_validation');

        $config = array(
            array(
                'field' => 'event-title',
                'label' => 'Title',
                'rules' => 'required',
                'errors' => array(
                    'required' => 'You have not provided %s.'
                )
            ),
            array(
                'field' => 'event-date',
                'label' => 'Date',
                'rules' => 'required',
                'errors' => array(
                    'required' => 'You have not provided the %s.'
                )
            ),
            array(
                'field' => 'event-time-start',
                'label' => 'Time Start',
                'rules' => 'required',
                'errors' => array(
                    'required' => 'You have to fill in the %s.'
                )
            ),
            array(
                'field' => 'event-time-end',
                'label' => 'Time End',
                'rules' => 'required',
                'errors' => array(
                    'required' => 'You have to fill in the %s.'
                )
            ),
            array(
                'field' => 'event-description',
                'label' => 'Description',
                'rules' => 'required',
                'errors' => array(
                    'required' => 'You have not provided %s.'
                )
            ),
        );

        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('templates/header');
            $this->load->view('mainpage/create_event');
            $this->load->view('templates/footer');
        } else {
            $this->SchedulerEventModel->insert_event();

            redirect('Anzen_Home');
        }
    }

    public function event_detail($id, $date)
    {
        //------------------------------------------CALENDAR--------------------------------------------//
        $prefs = array(
            'day_type' => 'long'
        );

        $prefs['template'] = '

            {table_open}<table border="0" cellpadding="0" cellspacing="0" class="calendar">{/table_open}

            {heading_row_start}<tr>{/heading_row_start}

            {heading_previous_cell}<th><a href="{previous_url}">&lt;&lt;</a></th>{/heading_previous_cell}
            {heading_title_cell}<th colspan="{colspan}">{heading}</th>{/heading_title_cell}
            {heading_next_cell}<th><a href="{next_url}">&gt;&gt;</a></th>{/heading_next_cell}

            {heading_row_end}</tr>{/heading_row_end}

            {week_row_start}<tr>{/week_row_start}
            {week_day_cell}<td>{week_day}</td>{/week_day_cell}
            {week_row_end}</tr>{/week_row_end}

            {cal_row_start}<tr class="days">{/cal_row_start}
            {cal_cell_start}<td>{/cal_cell_start}
            {cal_cell_start_today}<td>{/cal_cell_start_today}
            {cal_cell_start_other}<td class="other-month">{/cal_cell_start_other}

            {cal_cell_content}
                <div class="day_num">{day}</div>
                <div class="content">{content}</div>
            {/cal_cell_content}
            {cal_cell_content_today}
                <div class="day_num highlight">{day}</div>
                <div class="content">{content}</div>
            {/cal_cell_content_today}

            {cal_cell_no_content}
                <div class="day_num">{day}</div>
            {/cal_cell_no_content}
            {cal_cell_no_content_today}
                <div class="day_num highlight">{day}</div>
            {/cal_cell_no_content_today}

            {cal_cell_blank}&nbsp;{/cal_cell_blank}

            {cal_cell_other}{day}{/cal_cel_other}

            {cal_cell_end}</td>{/cal_cell_end}
            {cal_cell_end_today}</td>{/cal_cell_end_today}
            {cal_cell_end_other}</td>{/cal_cell_end_other}
            {cal_row_end}</tr>{/cal_row_end}

            {table_close}</table>{/table_close}
        ';

        $this->load->library('calendar', $prefs);

        //----------------------------------------------------------------------------------------------//

        //--------------------Grab all events that has the same month and year--------------------------//

        $temp['same_events'] = $this->SchedulerEventModel->get_all_events();
        $data['same_events'] = array();

        foreach ($temp['same_events'] as $loop) {
            if (substr($date, 0, 8) == substr($loop['date'], 0, 8)) {
                $data_each = explode("-", $loop['date']);
                array_push($data_each, $loop['title'], $loop['id'], $loop['date']);
                array_push($data['same_events'], $data_each);
            }
        }

        //----------------------------------------------------------------------------------------------//

        $time = explode("-", $date);
        $data['event'] = $this->SchedulerEventModel->get_event($id);

        $date_data = array();
        foreach ($data['same_events'] as $loop) {

            $temp = '<a href="' . site_url('Anzen_Home/event_detail/' . $loop[4]) . '/' . $loop[5] . '">' . $loop[3] . '</a>';

            if (substr($loop[5], 8, 2) < 10) {
                if (array_key_exists(substr($loop[5], 8, 2), $date_data)) {
                    $date_data[substr($loop[5], 9, 1)] = $date_data[substr($loop[5], 9, 1)] . "<br><br>" . $temp;
                } else {
                    $date_data[substr($loop[5], 9, 1)] = $temp;
                }
            } else {
                if (array_key_exists(substr($loop[5], 8, 2), $date_data)) {
                    $date_data[substr($loop[5], 8, 2)] = $date_data[substr($loop[5], 8, 2)] . "<br><br>" . $temp;
                } else {
                    $date_data[substr($loop[5], 8, 2)] = $temp;
                }
            }
        }

        $data['calendar'] = $this->calendar->generate($time[0], $time[1], $date_data);

        $this->load->view('templates/header');
        $this->load->view('mainpage/event_detail', $data);
        $this->load->view('templates/footer');
    }

    public function event_review($id, $date)
    {
        //------------------------------------------CALENDAR--------------------------------------------//
        $prefs = array(
            'day_type' => 'long'
        );

        $prefs['template'] = '

            {table_open}<table border="0" cellpadding="0" cellspacing="0" class="calendar">{/table_open}

            {heading_row_start}<tr>{/heading_row_start}

            {heading_previous_cell}<th><a href="{previous_url}">&lt;&lt;</a></th>{/heading_previous_cell}
            {heading_title_cell}<th colspan="{colspan}">{heading}</th>{/heading_title_cell}
            {heading_next_cell}<th><a href="{next_url}">&gt;&gt;</a></th>{/heading_next_cell}

            {heading_row_end}</tr>{/heading_row_end}

            {week_row_start}<tr>{/week_row_start}
            {week_day_cell}<td>{week_day}</td>{/week_day_cell}
            {week_row_end}</tr>{/week_row_end}

            {cal_row_start}<tr class="days">{/cal_row_start}
            {cal_cell_start}<td>{/cal_cell_start}
            {cal_cell_start_today}<td>{/cal_cell_start_today}
            {cal_cell_start_other}<td class="other-month">{/cal_cell_start_other}

            {cal_cell_content}
                <div class="day_num">{day}</div>
                <div class="content">{content}</div>
            {/cal_cell_content}
            {cal_cell_content_today}
                <div class="day_num highlight">{day}</div>
                <div class="content">{content}</div>
            {/cal_cell_content_today}

            {cal_cell_no_content}
                <div class="day_num">{day}</div>
            {/cal_cell_no_content}
            {cal_cell_no_content_today}
                <div class="day_num highlight">{day}</div>
            {/cal_cell_no_content_today}

            {cal_cell_blank}&nbsp;{/cal_cell_blank}

            {cal_cell_other}{day}{/cal_cel_other}

            {cal_cell_end}</td>{/cal_cell_end}
            {cal_cell_end_today}</td>{/cal_cell_end_today}
            {cal_cell_end_other}</td>{/cal_cell_end_other}
            {cal_row_end}</tr>{/cal_row_end}

            {table_close}</table>{/table_close}
        ';

        $this->load->library('calendar', $prefs);

        //----------------------------------------------------------------------------------------------//

        //--------------------Grab all events that has the same month and year--------------------------//

        $temp['same_events'] = $this->SchedulerEventModel->get_all_events();
        $data['same_events'] = array();

        foreach ($temp['same_events'] as $loop) {
            if (substr($date, 0, 8) == substr($loop['date'], 0, 8)) {
                $data_each = explode("-", $loop['date']);
                array_push($data_each, $loop['title'], $loop['id'], $loop['date']);
                array_push($data['same_events'], $data_each);
            }
        }

        //----------------------------------------------------------------------------------------------//

        $time = explode("-", $date);
        $data['event'] = $this->SchedulerEventModel->get_event($id);

        $date_data = array();
        foreach ($data['same_events'] as $loop) {

            $temp = '<a href="' . site_url('Anzen_Home/event_detail/' . $loop[4]) . '/' . $loop[5] . '">' . $loop[3] . '</a>';

            if (substr($loop[5], 8, 2) < 10) {
                if (array_key_exists(substr($loop[5], 8, 2), $date_data)) {
                    $date_data[substr($loop[5], 9, 1)] = $date_data[substr($loop[5], 9, 1)] . "<br><br>" . $temp;
                } else {
                    $date_data[substr($loop[5], 9, 1)] = $temp;
                }
            } else {
                if (array_key_exists(substr($loop[5], 8, 2), $date_data)) {
                    $date_data[substr($loop[5], 8, 2)] = $date_data[substr($loop[5], 8, 2)] . "<br><br>" . $temp;
                } else {
                    $date_data[substr($loop[5], 8, 2)] = $temp;
                }
            }
        }

        $data['calendar'] = $this->calendar->generate($time[0], $time[1], $date_data);

        $data['comments'] = $this->SchedulerCommentModel->get_all_comments($id);
        $data['event_id'] = $id;
        $data['event_date'] = $date;
        $this->session->set_flashdata('greet', true);

        $data['rating'] = $this->SchedulerRatingModel->get_rating($id);

        $this->load->view('templates/header');
        $this->load->view('mainpage/event_review', $data);
        $this->load->view('templates/footer');
    }

    public function write_comment($id, $date)
    {
        $this->load->library('form_validation');

        $config = array(
            array(
                'field' => 'comment-title',
                'label' => 'Title',
                'rules' => 'required',
                'errors' => array(
                    'required' => 'You have not provided %s.'
                )
            ),
            array(
                'field' => 'comment-description',
                'label' => 'Description',
                'rules' => 'required',
                'errors' => array(
                    'required' => 'You have not provided %s.'
                )
            ),
        );

        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('error_comment', "Please fill out the form after submitting");
        } else {
            $this->SchedulerCommentModel->insert_comment($id);
        }

        redirect('Anzen_Home/event_review/' . $id . '/' . $date);
    }

    public function write_rating($id, $date)
    {
        $rating = $this->SchedulerRatingModel->get_rating($id);

        if ($rating == NULL) {
            $this->SchedulerRatingModel->insert_rating($id);
        } else {
            $this->SchedulerRatingModel->update_rating($id);
        }

        redirect('Anzen_Home/event_review/' . $id . '/' . $date);
    }

    public function delete_event($id)
    {
        $this->SchedulerEventModel->del_event($id);

        redirect('Anzen_Home');
    }

    public function delete_comment($id, $eventId, $eventDate)
    {
        $this->SchedulerCommentModel->del_comment($id);

        redirect('Anzen_Home/event_review/' . $eventId . '/' . $eventDate);
    }

    public function delete_rating($id, $eventId, $eventDate)
    {
        $this->SchedulerRatingModel->del_rating($id);

        redirect('Anzen_Home/event_review/' . $eventId . '/' . $eventDate);
    }

    public function logout()
    {
        $array_items = array('username', 'email', 'logged_in');
        $this->session->unset_userdata($array_items);

        if ($this->input->cookie('username') != NULL && $this->input->cookie('email') != NULL) {
            $this->input->set_cookie("username", '', time() - 1);
            $this->input->set_cookie("email", '', time() - 1);
        }

        redirect('Anzen');
    }
}
