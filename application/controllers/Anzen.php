<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Anzen extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model('SchedulerEventModel');
        $this->load->model('SchedulerUserModel');
    }

    public function index()
    {
        $this->load->view('templates/header');
        $this->load->view('loginpage/login');
        $this->load->view('templates/footer');
    }

    public function login()
    {
        $this->load->library('form_validation');

        $config = array(
            array(
                'field' => 'username',
                'label' => 'Username',
                'rules' => 'required|min_length[5]',
                'errors' => array(
                    'required' => 'You have not provided %s.'
                )
            ),
            array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'required|valid_email',
                'errors' => array(
                    'required' => 'You have not provided %s.'
                )
            ),
            array(
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'required|min_length[6]',
                'errors' => array(
                    'required' => 'You must provide a %s.'
                )
            )
        );

        $this->form_validation->set_rules($config);

        $data['user'] = $this->SchedulerUserModel->check_user_exists($this->input->post('username'), $this->input->post('email'));

        foreach ($data['user'] as $temp) {
            $data['hash_password'] = $temp['password'];
            $data['auth_status'] = $temp['auth_status'];
        }

        if (!password_verify(strtolower($this->input->post('password')), $data['hash_password'])) {
            $data['password'] = false;
        }

        if ($data['user'] == NULL) {
            $this->session->set_flashdata('login_status', 'ERROR: Username and/or Email is incorrect! ');
            redirect('anzen');
        } elseif (isset($data['password'])) {
            $this->session->set_flashdata('login_status', 'ERROR: Password is incorrect!');
            redirect('anzen');
        } elseif ($data['auth_status'] == FALSE) {
            $this->session->set_flashdata('login_status', 'ERROR: You must verify your email first!');
            redirect('anzen');
        }

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('templates/header');
            $this->load->view('loginpage/login');
            $this->load->view('templates/footer');
        } else {
            $data['username'] = $this->input->post('username');
            $data['email'] = $this->input->post('email');
            $data['check'] = $this->input->post('check');

            if ($data['check'] != NULL) {
                $this->input->set_cookie("username", $data['username'], 60, "");
                $this->input->set_cookie("email", $data['email'], 60, "");
            }

            $userdata = array(
                'username' => $data['username'],
                'email' => $data['email'],
                'logged_in' => TRUE
            );

            $this->session->set_userdata($userdata);
            $this->session->set_tempdata('timeout', TRUE, 90);

            redirect('Anzen_Home');
        }
    }

    public function validate_captcha()
    {
        $captcha = $this->input->post('g-recaptcha-response');
        $response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6Ldluf0UAAAAACLJST1xEXnm9Ce2uFIuDQS-8e0x&response=" . $captcha . "&remoteip=" . $_SERVER['REMOTE_ADDR']);
        if ($response . 'success' == false) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function reg_open()
    {
        $this->load->view('templates/header');
        $this->load->view('loginpage/register');
        $this->load->view('templates/footer');
    }

    public function registration()
    {
        $this->load->library('form_validation');

        $config = array(
            array(
                'field' => 'register-username',
                'label' => 'Username',
                'rules' => 'required|min_length[5]',
                'errors' => array(
                    'required' => 'You have not provided %s.'
                )
            ),
            array(
                'field' => 'register-email',
                'label' => 'Email',
                'rules' => 'required|valid_email',
                'errors' => array(
                    'required' => 'You have not provided %s.'
                )
            ),
            array(
                'field' => 'register-phone',
                'label' => 'Phone Number',
                'rules' => 'numeric',
                'errors' => array(
                    'numeric' => '%s has to be a number'
                )
            ),
            array(
                'field' => 'reg-security-question',
                'label' => 'Security Question',
                'rules' => 'required'
            ),
            array(
                'field' => 'reg-security-answer',
                'label' => 'Security Answer',
                'rules' => 'required'
            ),
            array(
                'field' => 'register-password',
                'label' => 'Password',
                'rules' => 'required|min_length[6]',
                'errors' => array(
                    'required' => 'You must provide a %s.'
                )
            ),
            array(
                'field' => 'confirm-password',
                'label' => 'Confirmation Password',
                'rules' => 'required|matches[register-password]',
                'errors' => array(
                    'required' => 'You must provide a %s.',
                    'matches' => '%s does not match'
                )
            ),
            array(
                'field' => 'g-recaptcha-response',
                'label' => 'recaptcha validation',
                'rules' => 'required|callback_validate_captcha',
                'errors' => array(
                    'required' => 'Please check the the captcha form'
                )
            )
        );

        $this->form_validation->set_rules($config);

        $inputdata = array(
            'username' => $this->input->post('register-username'),
            'email' => $this->input->post('register-email')
        );

        $data = array(
            'username' => $this->SchedulerUserModel->username_exists($inputdata['username']),
            'email' => $this->SchedulerUserModel->email_exists($inputdata['email'])
        );

        if (!($data['username'] == NULL && $data['email'] == NULL)) {
            $this->session->set_flashdata('exists_status', 'ERROR: Username and/or Email has already exists!');
            redirect('anzen/reg_open');
        }

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('templates/header');
            $this->load->view('loginpage/register');
            $this->load->view('templates/footer');
        } else {
            $hash_password = password_hash(strtolower($this->input->post('register-password')), PASSWORD_DEFAULT);

            $this->SchedulerUserModel->insert_user($hash_password);

            $this->send_email($inputdata['email']);

            $this->session->set_flashdata('login_status', 'NOTIFICATION: You must verify your email before login in ');
            redirect('anzen');
        }
    }

    public function send_email($email)
    {
        $config = array(
            'protocol' => 'smtp',
            'smtp_host' => 'mailhub.eait.uq.edu.au',
            'smtp_port' => 25,
            'mailtype'  => 'html',
            'charset'   => 'iso-8859-1',
            'wordwrap'  => TRUE
        );
        $this->load->library('email', $config);

        $this->email->set_newline("\r\n");
        $this->email->from('noreply@infs3202-dd7f4b05.uqcloud.net');
        $this->email->to($email);

        $this->email->subject('Email Verification');
        $this->email->message("Hello,\n Click the link to verify your email address \n\n 
            https://infs3202-dd7f4b05.uqcloud.net/project/anzen/verify/" . $email . "\n\n Thanks \n Anzen Admin");
        $this->email->send();
    }

    public function verify($email = NULL)
    {
        $data['email'] = $this->SchedulerUserModel->email_exists($email);

        if ($data['email'] == NULL) {
            $this->session->set_flashdata('email_exists', false);
        } else {
            $this->session->set_flashdata('email_exists', true);
            $this->SchedulerUserModel->update_user_detail('email', $email, 'auth_status', TRUE);
        }

        redirect('anzen/verify_login');
    }

    public function verify_login()
    {
        if ($_SESSION['email_exists'] == false) {
            $this->session->set_flashdata('exists_status', 'ERROR: There is no account with this email!');
            redirect('anzen/reg_open');
        }

        $this->session->set_flashdata('login_status', 'SUCCESS: Your email is now Verified!');

        $this->load->view('templates/header');
        $this->load->view('loginpage/login');
        $this->load->view('templates/footer');
    }

    public function resend_open()
    {
        $this->load->view('templates/header');
        $this->load->view('loginpage/resend');
        $this->load->view('templates/footer');
    }

    public function resend_email()
    {
        $this->load->library('form_validation');

        $config = array(
            array(
                'field' => 'resend-email',
                'label' => 'Email',
                'rules' => 'required|valid_email',
                'errors' => array(
                    'required' => 'You have not provided %s.'
                )
            )
        );

        $input['email'] = $this->input->post('resend-email');

        if ($this->SchedulerUserModel->email_exists($input['email']) == NULL) {
            $this->session->set_flashdata('exists_status', 'ERROR: Email does not exists!');
            redirect('anzen/resend_open');
        }

        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('templates/header');
            $this->load->view('loginpage/resend');
            $this->load->view('templates/footer');
        } else {
            $this->send_email($input['email']);

            $this->session->set_flashdata('login_status', 'NOTIFICATION: Your email verification has been re-send');
            redirect('anzen');
        }
    }

    public function rec_open()
    {
        $this->load->helper('captcha');

        $vals = array(
            'img_path'      => './captcha/',
            'img_url'       => base_url() . 'captcha/',
            'font_path'     => './path/to/fonts/texb.ttf',
            'img_width'     => '200',
            'img_height'    => 60,
            'expiration'    => 30,
            'word_length'   => 8,
            'font_size'     => 16,
            'pool'          => '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
        );

        $captcha = create_captcha($vals);

        $this->session->unset_userdata('captchaCode');
        $this->session->set_userdata('captchaCode', $captcha['word']);

        $data['captImage'] = $captcha['image'];

        $this->load->view('templates/header');
        $this->load->view('loginpage/recover', $data);
        $this->load->view('templates/footer');
    }

    public function captcha_refresh()
    {
        $this->load->helper('captcha');

        $vals = array(
            'img_path'      => './captcha/',
            'img_url'       => base_url() . 'captcha/',
            'font_path'     => './path/to/fonts/texb.ttf',
            'img_width'     => '200',
            'img_height'    => 60,
            'expiration'    => 30,
            'word_length'   => 8,
            'font_size'     => 16,
            'pool'          => '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
        );

        $data = create_captcha($vals);

        $this->session->unset_userdata('captchaCode');
        $this->session->set_userdata('captchaCode', $data['word']);

        echo $data['image'];
    }

    public function recovery()
    {
        $this->load->library('form_validation');

        $config = array(
            array(
                'field' => 'recovery-username',
                'label' => 'Username',
                'rules' => 'required|min_length[5]',
                'errors' => array('required' => 'You have not provided %s.')
            ),
            array(
                'field' => 'recovery-email',
                'label' => 'Email',
                'rules' => 'required|valid_email',
                'errors' => array('required' => 'You have not provided %s.')
            ),
            array(
                'field' => 'recovery-question',
                'label' => 'Security Question',
                'rules' => 'required',
                'errors' => array('required' => 'You have not provided %s.')
            ),
            array(
                'field' => 'recovery-answer',
                'label' => 'Security Answer',
                'rules' => 'required',
                'errors' => array('required' => 'You have not provided %s.')
            )
        );

        $this->form_validation->set_rules($config);

        $inputdata = array(
            'username' => $this->input->post('recovery-username'),
            'email' => $this->input->post('recovery-email'),
            'captcha' => $this->input->post('recovery-captcha')
        );

        $sessCaptcha = $this->session->userdata('captchaCode');

        $data['user'] = $this->SchedulerUserModel->check_user_exists($inputdata['username'], $inputdata['email']);

        $tempdata = array(
            'question' => $this->input->post('recovery-question'),
            'answer' => $this->input->post('recovery-answer')
        );

        foreach ($data['user'] as $temp) {
            $data['question'] = $temp['security-question'];
            $data['answer'] = $temp['security-answer'];
        }

        if ($data['user'] == NULL) {
            $this->session->set_flashdata('recovery_status', 'ERROR: Username and/or Email does not exists!');
            redirect('anzen/rec_open');
        } elseif (!strcmp($data['question'], $tempdata['question']) == 0) {
            $this->session->set_flashdata('recovery_status', 'ERROR: Security Question does not match!');
            redirect('anzen/rec_open');
        } elseif (!strcmp($data['answer'], $tempdata['answer']) == 0) {
            $this->session->set_flashdata('recovery_status', 'ERROR: Security Answer does not match!');
            redirect('anzen/rec_open');
        } elseif (!strcmp($inputdata['captcha'], $sessCaptcha) == 0) {
            $this->session->set_flashdata('recovery_status', 'ERROR: Captcha is incorrect!');
            redirect('anzen/rec_open');
        }

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('templates/header');
            $this->load->view('loginpage/recover');
            $this->load->view('templates/footer');
        } else {
            $this->session->set_tempdata('temp_username', $inputdata['username'], 300);
            $this->session->set_tempdata('temp_email', $inputdata['email'], 300);

            redirect('anzen/pass_open');
        }
    }

    public function pass_open()
    {
        $this->load->view('templates/header');
        $this->load->view('loginpage/password');
        $this->load->view('templates/footer');
    }

    public function password_renewal()
    {
        $this->load->library('form_validation');

        $config = array(
            array(
                'field' => 'new-password',
                'label' => 'New Password',
                'rules' => 'required|min_length[6]',
                'errors' => array('required' => 'You have not provided %s.')
            ),
            array(
                'field' => 'confirm-new-password',
                'label' => 'Confirmation Password',
                'rules' => 'required|matches[new-password]',
                'errors' => array('required' => 'You have not provided %s.')
            )
        );

        $this->form_validation->set_rules($config);

        $data['user'] = $this->SchedulerUserModel->check_user_exists($_SESSION['temp_username'], $_SESSION['temp_email']);

        foreach ($data['user'] as $temp) {
            $data['hash_password'] = $temp['password'];
        }

        if (password_verify(strtolower($this->input->post('new-password')), $data['hash_password'])) {
            $data['password'] = 'true';
        }

        if (isset($data['password'])) {
            $this->session->set_flashdata('password_status', 'ERROR: Your password are the same as your previous one!');
            redirect('anzen/pass_open');
        }

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('templates/header');
            $this->load->view('loginpage/password');
            $this->load->view('templates/footer');
        } else {
            $hash_password = password_hash(strtolower($this->input->post('new-password')), PASSWORD_DEFAULT);

            $this->SchedulerUserModel->update_user('password', $data['hash_password'], $hash_password);

            $this->session->set_flashdata('login_status', 'SUCCESS: Your Password has been re-new!');
            redirect('anzen');
        }
    }
}
